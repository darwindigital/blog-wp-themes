<?php 
/* 
 * Template Name: template-contact 
 */ 
?>
        <?php get_header(); ?>
        <div class="template-contact-container">
            <div class="template-contact-wrapper">
                <div class="template-contact-map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d73059.52657468968!2d6.605918772513216!3d46.495460418392526!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x478c2e3159fa0d15%3A0x24f8bb81fcfefffa!2sLausanne+Cathedral!5e0!3m2!1sen!2sch!4v1557923780778!5m2!1sen!2sch" width="1127" height="925" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                
                <div class="template-contact-form" role="main">
                    <h4>Contact Us</h4>
                    <h1>Let's Talk</h4>
                    <p>Our friendly custom service team always respond to enquiries within 24 hours.</p>
                    <title><?php wp_title( '|', false, 'right' ); ?></title>
                    <form action="/action_page.php" target="_blank">
                        <input type="text" name="name" placeholder="Name"/>
                        <input type="text" name="email"  placeholder="hello@yourbrand.com" />
                        <textarea name="message" placeholder="Message..."></textarea>
                        <input type="submit" name="submit" value="Send"/>
                    </form>
                </div>
            </div>
        </div>
        <?php get_footer(); ?>
    </body>
</html>