<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 */
if ( ! function_exists( 'blankperso1_social_menu' ) ) :
	/**
	 * Prints HTML with meta information for the current post-date/time.
	 */
    function blankperso1_social_menu() {
        if ( has_nav_menu( 'social' ) ) {
            wp_nav_menu(
                array(
                    'theme_location'  => 'social',
                    'container'       => 'div',
                    'container_id'    => 'menu-social',
                    'container_class' => 'menu-social',
                    'menu_id'         => 'menu-social-items',
                    'menu_class'      => 'menu-items',
                    'depth'           => 1,
                    'link_before'     => '<span class="screen-reader-text">',
                    'link_after'      => '</span>',
                    'fallback_cb'     => '',
                )
            );
        }
    }
endif;