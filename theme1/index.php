
            <?php get_header(); ?>

            <div class="index-container">
                <div class="index-header">
                <?php
                    $sticky = get_option( 'sticky_posts' );
                    $args = array(
                            'posts_per_page' => 1,
                            'post__in'  => $sticky,
                            'ignore_sticky_posts' => 1
                    );
                    $query = new WP_Query( $args );
                    if ( isset( $sticky[0] ) ) {
                ?>      <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>> 
                            <?php
                                the_post_thumbnail();
                                $post = get_post();
                                $shortlink = wp_get_shortlink( $post->ID );
                            ?>
                                <div class="sticky-containt">
                                    <?php
                                        the_category();
                                        echo '<p>' . get_the_title() . '</p>';
                                        echo '<a rel="shortlink" href="' . esc_url( $shortlink ) . '">
                                            <button>
                                                Read more
                                                <i class="fal fa-arrow-right"></i>
                                            </button>
                                        </a>'
                                    ?>
                                </div>
                        </div>
                <?php
                    }
                    wp_reset_postdata();
                ?>
                </div>

                <div class="index-containt">
                    <div class="index-category">
                        <div class="index-category-header">
                            <p>
                                Categories
                            </p>
                            <a href="javascript:void(0);" class="category-list-expand">
                                <i class="fal fa-plus"></i>
                            </a>
                        </div>
                    </div>

                    <div class="index-category-list">
                        <?php
                            wp_list_categories( array(
                                'orderby' => 'count',
                                'hide_empty' => 0,
                                'title_li' => '',
                                'show_option_none' => 'Nothing found, try to add categories!'
                            ));
                        ?>
                    </div>

                    <div class="index-posts-row">
                        <?php
                            $the_query = new WP_Query( array( 'post__not_in' => get_option( 'sticky_posts' ) ) );
                            $column_return = 0;
                            $count_posts = wp_count_posts();
                            $published_posts = $the_query->post_count;
                            $read_more = false;

                            if ( $published_posts > 8 ) {
                                $post_per_column = 4;
                                $read_more = true;
                            } else {
                                $post_per_column = round($published_posts / 2);
                            }
                            
                            $post_five = NULL;
                            while($column_return < 2) {
                        ?>
                            <div class="index-posts-column">
                                <?php
                                    $post_count = 0;

                                    if ( !empty($post_five) ) {
                                        $post = get_post( $post_five );
                                        $post_id = $post->ID;
                                        $shortlink = wp_get_shortlink( $post_id );

                                        if ( has_post_thumbnail( $post )) {
                                        ?>       
                                            <div class="index-post">
                                                <?php the_post_thumbnail(); ?>
                                        
                                                <div class="index-post-containt">
                                                    <?php
                                                        echo '<a rel="shortlink" href="' . esc_url( $shortlink ) . '">' . get_the_title() . '</a>';
                                                        the_category();
                                                    ?>
                                                </div>
                                            </div>
                                        <?php  
                                        } else {
                                            ?>              
                                            <!--<div class="index-post-no-thumbnail">-->
                                                <!--<div class="index-post-containt">-->
                                                    <?php
                                                        // echo '<a rel="shortlink" href="' . esc_url( $shortlink ) . '">' . get_the_title() . '</a>';
                                                        // the_category();
                                                    ?>
                                                <!--</div>-->
                                            <!--</div>-->
                                        <?php
                                        }
                                    }
                                    
                                    if ( $the_query->have_posts() ) : 
                                        while ( $the_query->have_posts() ) : $the_query->the_post();
                                            if($post_count == $post_per_column) {
                                                $post_five = get_post()->ID;
                                                break;
                                            }
                                            else {
                                                $post = get_post();
                                                $post_id = $post->ID;
                                                $shortlink = wp_get_shortlink( $post_id );
                                                $post_count++;

                                                if ( has_post_thumbnail( $post )) {
                                ?>       
                                                    <div class="index-post">
                                                        <?php the_post_thumbnail(); ?>
                                                
                                                        <div class="index-post-containt">
                                                            <?php
                                                                echo '<a rel="shortlink" href="' . esc_url( $shortlink ) . '">' . get_the_title() . '</a>';
                                                                the_category();
                                                            ?>
                                                        </div>
                                                    </div>
                                <?php  
                                                } else {
                                    ?>              <!--<div class="index-post-no-thumbnail">-->
                                                        <!--<div class="index-post-containt">-->
                                                            <?php
                                                                // echo '<a rel="shortlink" href="' . esc_url( $shortlink ) . '">' . get_the_title() . '</a>';
                                                                // the_category();
                                                            ?>
                                                        <!--</div>-->
                                                    <!--</div>-->
                                <?php
                                                }
                                            }
                                            endwhile;
                                            else :                                                                      
                                                // When no posts are found, output this text.                           
                                                _e( 'Sorry, no posts matched your criteria.' ); 
                                ?>
                        <?php 
                                    endif;
                                    $column_return++;
                        ?>
                            </div>
                        <?php
                            }
                            wp_reset_postdata();
                        ?>
                    </div>

                    <?php
                        if ( $read_more ) {
                    ?>
                            <div class="index-read-more">
                                <a href="javascript:void(0)">See more</a>
                            </div>
                    <?php
                        }
                    ?>
                </div>
            </div>

            <?php get_footer(); ?>
        </div>
    </body>
</html>
