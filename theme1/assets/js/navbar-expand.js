jQuery(document).ready(function($) {
    // HOME CATEGORY LIST
    $(".category-list-expand").click(function(){
        $(".index-category-list").slideToggle(200, "linear");
        $list_icon = $(".category-list-expand i");

        if ($list_icon.hasClass("fa-plus")) {
            $list_icon.removeClass('fa-plus');
            $list_icon.addClass('fa-minus');
        } else {
            $list_icon.addClass('fa-plus');
            $list_icon.removeClass('fa-minus');
        }
    });

    // NAVBAR MOBILE
    $(".navbar-mobile-icon").click(function(){
        $(".navbar-expand").slideToggle(200, "linear");
        $menu_icon = $(".navbar-mobile-icon i");

        if ($menu_icon.hasClass("fa-bars")) {
            $menu_icon.removeClass('fa-bars');
            $menu_icon.addClass('fa-times');
        } else {
            $menu_icon.addClass('fa-bars');
            $menu_icon.removeClass('fa-times');
        }
    });

    // FOOTER MOBILE
    $(".footer-mobile-links").click(function(){
        $(".mobile-links-containt").slideToggle(10, "linear");
        
        // check if another tab is already open and close it
        if ($(".site-footer-mobile").hasClass("contact-active")) {
            $(".mobile-contact-containt").slideToggle(10, "linear");
            $(".site-footer-mobile").removeClass("contact-active");
        }
        else if ($(".site-footer-mobile").hasClass("sign-active")) {
            $(".mobile-sign-up-containt").slideToggle(10, "linear");
            $(".site-footer-mobile").removeClass("sign-active");
        }
        
        // check if clicked tab is opened or not
        if ($(".site-footer-mobile").hasClass("links-active")) {
            $(".site-footer-mobile").css("grid-template-rows", "repeat(3, 50px) 75px 75px");
            $(".site-footer-mobile").removeClass("links-active");
        }
        else {
            $(".site-footer-mobile").css("grid-template-rows", "50px 50px auto 50px 75px 75px");
            $(".site-footer-mobile").addClass("links-active");
        }
    });

    $(".footer-mobile-contact").click(function(){
        $(".mobile-contact-containt").slideToggle(10, "linear");

        // check if another tab is already open and close it
        if ($(".site-footer-mobile").hasClass("links-active")) {
            $(".mobile-links-containt").slideToggle(10, "linear");
            $(".site-footer-mobile").removeClass("links-active");
        }
        else if ($(".site-footer-mobile").hasClass("sign-active")) {
            $(".mobile-sign-up-containt").slideToggle(10, "linear");
            $(".site-footer-mobile").removeClass("sign-active");
        }

        // check if clicked tab is opened or not
        if ($(".site-footer-mobile").hasClass("contact-active")) {
            $(".site-footer-mobile").css("grid-template-rows", "repeat(3, 50px) 75px 75px");
            $(".site-footer-mobile").removeClass("contact-active");
        }
        else {
            $(".site-footer-mobile").css("grid-template-rows", "50px auto 50px 50px 75px 75px");
            $(".site-footer-mobile").addClass("contact-active");
        }
    }); 

    $(".footer-mobile-sign-up").click(function(){
        $(".mobile-sign-up-containt").slideToggle(10, "linear");

        // check if another tab is already open and close it
        if ($(".site-footer-mobile").hasClass("links-active")) {
            $(".mobile-links-containt").slideToggle(10, "linear");
            $(".site-footer-mobile").removeClass("links-active");
        }
        else if ($(".site-footer-mobile").hasClass("contact-active")) {
            $(".mobile-contact-containt").slideToggle(10, "linear");
            $(".site-footer-mobile").removeClass("contact-active");
        }

        // check if clicked tab is opened or not
        if ($(".site-footer-mobile").hasClass("sign-active")) {
            $(".site-footer-mobile").css("grid-template-rows", "repeat(3, 50px) 75px 75px");
            $(".site-footer-mobile").removeClass("sign-active");
        }
        else {
            $(".site-footer-mobile").css("grid-template-rows", "50px 50px 50px auto 75px 75px");
            $(".site-footer-mobile").addClass("sign-active");
        }
    }); 
});