<footer>
    <div class="site-footer">
        <div class="footer-containt">
            <p class="footer-column-title">Helpful Links</p>
            <div class="footer-links">
                <p class="footer-links-containt">
                    About us<br>
                    Delivery and Returns Policy<br>
                    Help & FAQ<br>
                    Service for professionals<br>
                </p>

                <p class="footer-links-containt">
                    About us<br>
                    Delivery and Returns Policy<br>
                    Help & FAQ<br>
                    Service for professionals<br>
                </p>
            </div>
            
            <p class="footer-column-title">Contact Info</p>
            <div class="footer-address">
                <p class="footer-address-title">
                    Oxford (UK)
                </p>
                <p class="footer-address-containt">
                    1-3 Abbey Street<br>
                    Eynsham<br>
                    Oxford<br>
                    OX29 4TB<br>
                </p>

                <p class="footer-address-title">
                    Walnut, CA (USA)
                </p>
                <p class="footer-address-containt">
                    340 S Lemon Ave #3358<br>
                    Walnut<br>
                    California 91789<br>
                    USA<br>
                </p>
            </div>

            <p class="footer-column-title sign-up">Sign Up Now</p>
            <form action="/action_page.php" target="_blank">
                <label for="name">Name</label>
                <br>
                <input type="text" name="name">
                <br>
                <label for="email">Email</label>
                <input type="email" name="email">
                <br>
                <input type="submit" value="Sign up now">
            </form> 
        </div>
        <div class="footer-copyright">
            <?php printf( esc_html__( '&copy; %1$s %2$s. Designed by Tran Mau Tri Tam', 'blankperso1' ), date( 'Y' ), get_bloginfo( 'name' ) ); ?>

            <div class="footer-social">
                <?php blankperso1_social_menu(); ?>
            </div>
        </div>
    </div>

    <div class="site-footer-mobile">
        <a href="javascript:void(0)" class="footer-column-title footer-mobile-contact">
            Contact Info
            <i class="fas fa-angle-down"></i>
        </a>
            <ul class="mobile-contact-containt">
                <li>
                    <p class="footer-address-title">
                        Oxford (UK)
                    </p>
                    <p class="footer-address-containt">
                        1-3 Abbey Street<br>
                        Eynsham<br>
                        Oxford<br>
                        OX29 4TB<br>
                    </p>
                </li>
                <li>
                    <p class="footer-address-title">
                        Walnut, CA (USA)
                    </p>
                    <p class="footer-address-containt">
                        340 S Lemon Ave #3358<br>
                        Walnut<br>
                        California 91789<br>
                        USA<br>
                    </p>
                </li>
            </ul>
            
        <a href="javascript:void(0)" class="footer-column-title footer-mobile-links">
            Helpful Links
            <i class="fas fa-angle-down"></i>
        </a>
            <ul class="mobile-links-containt">
                <li>About us</li>
                <li>Delivery and Returns Policy</li>
                <li>Help & FAQ</li>
                <li>Service for professionals</li>
            </ul>

        <a href="javascript:void(0)" class="footer-column-title footer-mobile-sign-up">
            Sign Up
            <i class="fas fa-angle-down"></i>
        </a>
            <form action="/action_page.php" target="_blank" class="mobile-sign-up-containt">
                <label for="name">Name</label>
                <br>
                <input type="text" name="name">
                <br>
                <label for="email">Email</label>
                <input type="email" name="email">
                <br>
                <input type="submit" value="Sign up now">
            </form> 

        <div class="footer-social">
            <?php blankperso1_social_menu(); ?>
        </div>

        <div class="footer-copyright-mobile">
            <?php printf( esc_html__( '&copy; %1$s %2$s. Designed by Tran Mau Tri Tam', 'blankperso1' ), date( 'Y' ), get_bloginfo( 'name' ) ); ?>
        </div>
     </div>
</footer>

<?php wp_footer(); ?>