<?php
/**
 * The template for displaying all single posts and attachments
 *
 */
 
?>
    <?php get_header(); ?>
    
    <div class="single-container">
    
        <?php
        // Start the loop.
        while ( have_posts() ) : the_post();
            echo '<h1>' . get_the_title() . '</h1>'; 

            echo '<h4>' . get_the_content() . '</h4>';
        ?>

        <div class="single-thumbnail">
            <?php
                the_post_thumbnail();
            ?>
        </div>

        <?php
            /*
             * Include the post format-specific template for the content. If you want to
             * use this in a child theme, then include a file called called content-___.php
             * (where ___ is the post format) and that will be used instead.
             */
            get_template_part( 'content', get_post_format() );
 
            // If comments are open or we have at least one comment, load up the comment template.
            if ( comments_open() || get_comments_number() ) :
                comments_template();
            endif;
 
            // Previous/next post navigation.
            the_post_navigation( array(
                'prev_text' => __( '<p> Previous Post <br>%title</p>' ),
                'next_text' => __( '<p> Next Post <br>%title</p>' ),
            ) );
 
        // End the loop.
        endwhile;
        ?>
 
    </div><!-- .content-area -->

    <?php get_footer(); ?>
