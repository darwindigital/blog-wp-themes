<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php wp_title( '|', true, 'right' ); ?></title>
        <link rel="stylesheet" href="<?php echo esc_url( get_stylesheet_uri() ); ?>" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet"> 
        <?php wp_head(); ?>
    </head>

    <body>
        <div id="page" class="site">
            <header>
                <div class="site-navbar">
                    <?php if ( has_custom_logo() ) : ?>
                        <div class="navbar-logo">
                            <?php 
                                if ( function_exists( 'the_custom_logo' ) ) {
                                    the_custom_logo();
                                }
                            ?>
                        </div>
                    <?php endif; ?>

                    <?php wp_nav_menu( 
                        array( 
                            'theme_location' => 'primary', 
                            'echo' => true,
                            'items_wrap' => '<ul>%3$s</ul>'
                        ) 
                    );
                    ?>

                    <div class="navbar-search">
                        <div class="search-box">
                            <input type="text" placeholder="Search" class="search-box-input">
                        </div>

                        <div class="search-btn">
                            <i class="fa fa-search" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>

                <div class="site-navbar-mobile">
                    <?php if ( has_custom_logo() ) : ?>
                        <div class="navbar-logo">
                            <?php 
                                if ( function_exists( 'the_custom_logo' ) ) {
                                    the_custom_logo();
                                }
                            ?>
                        </div>
                    <?php endif; ?>
                    
                    <a href="javascript:void(0);" class="navbar-mobile-icon">
                        <i class="fa fa-bars"></i>
                    </a>

                    <div class="navbar-search">
                        <div class="search-box">
                            <input type="text" placeholder="Search" class="search-box-input">
                        </div>
                        
                        <div class="search-btn">
                            <i class="fa fa-search" aria-hidden="true"></i>
                        </div>
                    </div>

                    <div class="navbar-expand">
                    <?php   
                        wp_nav_menu( 
                            array( 
                                'theme_location' => 'primary', 
                                'echo' => true,
                                'items_wrap' => '<ul>%3$s</ul>'
                            ) 
                        );
                    ?>
                    </div>
                </div>
            </header>