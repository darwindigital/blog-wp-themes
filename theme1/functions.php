<?php
/**
 * Fooding functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 */

if ( ! function_exists( 'blankperso1_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function blankperso1_setup() {
    // Add default posts and comments RSS feed links to head.
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'automatic-feed-links' );
    add_theme_support( 'custom-logo', array(
        'height'      => 100,
        'width'       => 400,
        'flex-height' => true,
        'flex-width'  => true,
        'header-text' => array( 'site-title', 'site-description' ),
    ) );
} endif;
add_action( 'after_setup_theme', 'blankperso1_setup' );

function register_my_menus() {
    register_nav_menus(
        array(
            'primary' => esc_html__( 'Primary', 'blankperso1' ),
            'social' => esc_html__('Social', 'blankperso1'),
        )
    );
}
add_action( 'init', 'register_my_menus' );

function blankperso1_scripts() {
    wp_enqueue_script( 'navbar-search-button', get_template_directory_uri() . '/assets/js/navbar-search-button.js', array( 'jquery' ), '1.0.0', true );
    wp_enqueue_script( 'navbar-expand', get_template_directory_uri() . '/assets/js/navbar-expand.js', array( ), '1.1', 'all' );
    wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/assets/css/font-awesome.min.css', array(), '4.7' );
}
add_action( 'wp_enqueue_scripts', 'blankperso1_scripts' );

function blankperso1_custom_logo_setup() {
    $defaults = array(
    'height'      => 400,
    'width'       => 400,
    'flex-height' => true
    );
    add_theme_support( 'custom-logo', $defaults );
}
add_action( 'after_setup_theme', 'blankperso1_custom_logo_setup' );

require get_template_directory() . '/inc/template-tags.php';