jQuery(document).ready(function($) {
    function index_responsiveness( $media_query ) {
        $logo_nh = $('.logo-nh');
        $logo = $('.logo');
        $post = $('.index-gallery article');

        // MAX WIDTH 767
        if (window.matchMedia('(' + $media_query + ')').matches) {
            if ($media_query == "max-width: 767px") {
                // navbar not home
                $logo_nh.addClass('col-2');
                $logo_nh.removeClass('col-1');
        
                // navbar home
                $logo.addClass('col-2');
                $logo.removeClass('col-1');

                if ($post.hasClass('col-6')) {
                    $post.addClass('col-12');
                }
            } 
            else if ($media_query == "min-width: 768px") {
                if ($post.hasClass('col-6') && $post.hasClass('col-12')) {
                    $post.removeClass('col-12');
                }
            }
            
        }

        // MAX WIDTH 767
        $(window).resize(function(){
            if (window.matchMedia('(' + $media_query + ')').matches) {
                if ($media_query == "max-width: 767px") {
                    // navbar not home
                    $logo_nh.addClass('col-2');
                    $logo_nh.removeClass('col-1');

                    // navbar home
                    $logo.addClass('col-2');
                    $logo.removeClass('col-1');

                    if ($post.hasClass('col-6')) {
                        $post.addClass('col-12');
                    }
                }
                else if ($media_query == "min-width: 768px") {
                    // navbar not home
                    $logo_nh.addClass('col-1');
                    $logo_nh.removeClass('col-2');

                    // navbar home
                    $logo.addClass('col-1');
                    $logo.removeClass('col-2');

                    if ($post.hasClass('col-6') && $post.hasClass('col-12')) {
                        $post.removeClass('col-12');
                    }
                }
            }
        });
    }

    index_responsiveness( 'max-width: 767px' );
    index_responsiveness( 'min-width: 768px' );
});