<?php
/**
 * Darwin blog functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 */

if ( ! function_exists( 'darwinblog_setup' ) ) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function darwinblog_setup() {
        // Add thumbnails option on posts and comments RSS feed links to head.
        add_theme_support( 'post-thumbnails' );
        add_theme_support( 'automatic-feed-links' );
    } 
endif;
add_action( 'after_setup_theme', 'darwinblog_setup' );

function register_my_menus() {
    register_nav_menus(
        array(
            'primary' => esc_html__( 'Primary', 'darwinblog' ),
            'social' => esc_html__('Social', 'darwinblog'),
        )
    );
}
add_action( 'init', 'register_my_menus' );

function darwinblog_scripts() {
    wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/css/bootstrap-grid.min.css', array(), '4.3.0' );
    wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/assets/css/font-awesome.min.css', array(), '4.7' );
    wp_enqueue_script( 'responsive', get_template_directory_uri() . '/assets/js/responsive.js', array('jquery'));
}
add_action( 'wp_enqueue_scripts', 'darwinblog_scripts' );

function darwinblog_load_theme_textdomain() {
    load_theme_textdomain( 'darwinblog', get_template_directory() . '/languages' );
}
add_action( 'darwinblog', 'darwinblog_load_theme_textdomain' );

function wpb_add_googleanalytics() {

    // Paste your Google Analytics tracking code here

}
add_action('wp_head', 'wpb_add_googleanalytics');

require get_template_directory() . '/inc/template-tags.php';
?>