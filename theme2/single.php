<?php
/**
 * The template for displaying all single posts and attachments
 *
 */
 
?>
    <?php get_header(); ?>
    
    <main class="container single-page">
        <?php while ( have_posts() ) : the_post(); ?>
        <header class="single-header">
            <figure>
                <?php
                $post = get_post();
                $post_id = $post->ID;
                $bg_hex = "";

                if ( has_post_thumbnail( $post )) {
                    the_post_thumbnail();
                }
                else {
                    ?>
                    <span style="background: <?php echo $post->bg; ?>"></span>
                    <?php
                }
                ?>
                <h2><?php echo get_the_title(); ?></h2>
            </figure>
        </header>
        <article class="container single-containt">
            <h1><?php echo get_the_title(); ?></h1>
            <h4><?php echo get_the_excerpt(); ?></h4>
            <?php the_content(); ?>
        </article>
        <?php endwhile; ?>
    </main>

    <?php get_footer(); ?>