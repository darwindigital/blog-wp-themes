<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="image" content="screenshot.png">
        <meta name="description" content="<?php echo (get_bloginfo( 'description' ) ? get_bloginfo( 'description' ) : _e('An IT Student demo WordPress theme, but with your photos, and your stories.', 'darwinblog')); ?>">

        <link rel="shortcut icon" href="favicon.ico" />
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri() ."/assets/images/apple-touch-icon.png" ?>">
        <link rel="icon" sizes="192x192" href="<?php echo get_template_directory_uri() ."/assets/images/android-chrome-192x192.png" ?>">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri() ."/assets/images/favicon-32x32.png" ?>">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri() ."/assets/images/favicon-16x16.png" ?>">
        <link rel="manifest" href="<?php echo get_template_directory_uri() ."/assets/images/site.webmanifest" ?>">
        <link rel="mask-icon" href="<?php echo get_template_directory_uri() ."/assets/images/safari-pinned-tab.svg" ?>" color="#548bf8">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="theme-color" content="#ffffff">

        <title><?php !is_front_page() ? bloginfo('name') . wp_title('-') : bloginfo('name'); ?></title>
        <link rel="stylesheet" href="<?php echo esc_url( get_stylesheet_uri() ); ?>" type="text/css"/>
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&display=swap" rel="stylesheet">
        <?php wp_head(); ?>
    </head>

    <body>
        <header>
            <?php   
            $logo = "logo";
            $navbar = "navbar";
            $navbar_menu = "navbar-menu";
            $navbar_type = "navbar-home";

            if ( !is_front_page() || !is_home() ) :
                $logo = "logo-nh";
                $navbar = "navbar-nh";
                $navbar_menu = "navbar-nh-menu";
                $navbar_type = "navbar-single";
            endif;
            ?>

            <nav class="container <?php echo $navbar_type ?>">
                <div class="row no-gutters <?php echo $navbar ?>">
                    <a href="<?php echo esc_url( home_url( '/' ) ) ?>" class="col-1 <?php echo $logo ?>">
                        <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 81 97.9">
                                <linearGradient id="gradient" gradientUnits="userSpaceOnUse" x1="0" x2="0" y1="0" y2="100">
                                    <stop offset="25%" stop-color="#548bf8" /> <!-- Colors to transition from -->
                                    <stop offset="75%" stop-color="#f9585c" />
                                </linearGradient>
                                <title>main</title>
                                <path d="M59.47,55.66c-.37-.32-15.29-11.14-15.29-11.14l0-.05c-.58-.58-.54-1.08-.59-1.95,0,0-.09-7.17-.22-9.56s-1.39-4-2.84-4-2.69,1.56-2.83,4-.23,9.56-.23,9.56c0,.87,0,1.37-.58,2l-.05,0S21.88,55.33,21.5,55.65a2.23,2.23,0,0,0-1,1.89c0,1.27.38,1.42,1.1,1.21,0,0,14.57-4.11,15.11-4.24s.82.06.86.78.08,4.82.1,5.29-.07.49-.31.77l-3.32,4.24a1.15,1.15,0,0,0-.28.76v1.29c0,.65.29.76.85.51S38,66.34,38,66.34a2.6,2.6,0,0,1,.66-.2c.44,0,.83,0,.92.65.13,1.18.51,2.11.88,2.11s.75-.93.88-2.11c.08-.66.48-.65.92-.65a2.6,2.6,0,0,1,.66.2s2.83,1.56,3.39,1.81.85.14.85-.51V66.35a1.13,1.13,0,0,0-.28-.76L43.6,61.35a.83.83,0,0,1-.31-.77c0-.46.06-4.58.1-5.29s.32-.92.86-.78,15.11,4.24,15.11,4.24c.73.21,1.1.06,1.1-1.21A2.32,2.32,0,0,0,59.47,55.66Z"/>
                        </svg>
                    </a>

                    <aside class="col-auto <?php echo $navbar_menu ?>">
                        <?php darwinblog_social_menu(); ?>
                    </aside>
                </div>
            </nav>
        </header>