    <?php 
        get_header();
    ?>

    <main class="container index-page">
        <section class="col-12 row no-gutters index-header">
            <h1 class="col-8"><?php echo (get_bloginfo( 'description' ) ? get_bloginfo( 'description' ) : _e('An IT Student demo WordPress theme, but with your photos, and your stories.', 'darwinblog')); ?></h1>
        </section>

        <section class="row no-gutters index-gallery">
            <?php
                // get 6 posts (except sticky posts)
                $the_query = new WP_Query( array( 
                        'post__not_in' => get_option( 'sticky_posts' ),
                        'post_status' => 'publish',
                        'orderby' => array(
                            'date' =>'DESC',
                            'menu_order'=>'ASC',
                        ),
                        'posts_per_page' => 6
                    ) 
                );
                $post_count = 0;
                $col_number = 0;
                $post_bg_array = array(
                    "#f1c40f",
                    "#548bf8",
                    "#27ae60",
                    "#f9585c",
                    "#1abc9c",
                    "#e67e22"
                );

                // Add custom size to prevent image from auto scaling
                add_image_size('post-size', 1024, 320);
            
                if ( $the_query->have_posts() ) : 
                    while ( $the_query->have_posts() ) : $the_query->the_post();
                        // change col for post 3 and 6 (col-12)
                        if($post_count == 2 || $post_count == 5) {
                            $col_number = 12;
                        }
                        else {
                            $col_number = 6;
                        }

                        $post = get_post();
                        $post_id = $post->ID;
                        $shortlink = wp_get_shortlink( $post_id );
                        $post_bg = $post_bg_array[$post_count];
                        // Add custom post meta to use background value on multiple pages
                        if ( !add_post_meta( $post_id, 'bg', $post_bg, true )) {
                            update_post_meta( $post_id, 'bg', $post_bg);
                        }
                        else {
                            add_post_meta( $post_id, 'bg', $post_bg, true );
                        }
            ?>
                        <article class="col-<?php echo $col_number; ?>" style="background: <?php echo $post->bg; ?>">
                            <?php 
                            if ( has_post_thumbnail( $post )) {
                                the_post_thumbnail( 'post-size' );
                            }
                            ?>

                            <div class="post-containt">
                                <!-- give post number to single page as $_GET variable to display assiocated bg-color if no thumbnail -->
                                <h2><?php echo '<a href="' . esc_url( $shortlink ) . '">' . get_the_title() . '</a>'; ?></h2>
                                <h4><?php echo get_the_excerpt(); ?></h4>
                            </div>
                        </article>
            <?php
                        $post_count++;
                    endwhile;
                    else :                                                                      
                        // When no posts are found, output this text.                           
                        _e( 'Sorry, no posts matched your criteria.' ); 
                endif;
            ?>
        </section>

        <section class="container index-contact">
            <div class="contact-wrapper">
                <h1><?php _e('Want to use any of these photos? Go for it!', 'darwinblog') ?></h1>
                <a class="contact-button" href="http://www.pexels.com" target="_blank"><?php _e('Get them here!', 'darwinblog') ?></a>
            </div>
        </section>
    </main>

    <?php
        get_footer();
    ?>