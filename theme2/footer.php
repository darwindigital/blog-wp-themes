        <footer class="container site-footer">    
            <div class="row no-gutters footer-wrapper">
                <figure class="footer-signature">   
                    <img alt="Author Signature" src=" <?php echo get_template_directory_uri() . "/assets/images/footer-signature.png" ?>">
                </figure>

                <figure class="col-12 row no-gutters footer-author">
                    <?php
                        echo get_avatar( get_the_author_meta( 'ID' ), 128 );
                    ?>
                    
                    <figcaption class="col-12">
                        <?php the_author(); ?>
                    </figcaption>
                </figure>
            </div>
        </footer>
        <?php wp_footer(); ?>
    </body>
</html>